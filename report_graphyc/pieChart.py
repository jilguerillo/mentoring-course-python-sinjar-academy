import matplotlib.pyplot as pyplot
import numpy as np

'''
source : https://matplotlib.org/stable/gallery
'''

data = [50,20,10,15]
labels = 'Python', 'Javascript','C#', 'Rust'
explode = (0, 0.1, 0, 0)

fig1, ax1 = pyplot.subplots()


ax1.pie(data, explode=explode, labels = labels, 
            autopct='%1.1f%%', startangle=90, shadow = True)

ax1.axis('equal')

pyplot.show()