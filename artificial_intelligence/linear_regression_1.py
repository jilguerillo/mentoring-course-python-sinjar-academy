#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil

linear regresion 1

𝑦 = 𝑎𝑥 + 𝑏
"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

rng = np.random.RandomState(1)
x = 10 * rng.randn(50)
y = 2 * x - 5 + rng.randn(50)
plt.scatter(x, y)

"""
Next, imported and used the LinearRegression model from Scikit-Learn to find the parameters that best fit the data and find the line: "LinearRegression".
the parameters that best fit the data and find the line:
    
"""

# import the model
model = LinearRegression(fit_intercept=True)

# the model find its parameters
model.fit(x[:, np.newaxis], y)

# Generate data
xfit = np.linspace(0, 10, 1000)

# Use the model for prediction value on the domain
yfit = model.predict(xfit[:, np.newaxis])

# Plots
plt.scatter(x, y)
plt.plot(xfit, yfit)
plt.show()

# Parameters
print("Slope of the straight line : ", model.coef_[0])
print("Intercept of the line : ", model.intercept_)







