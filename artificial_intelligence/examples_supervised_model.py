#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil

the Iris dataset, analyzed by Ronald Fisher in 1936, where 3 types of flowers (ketoses, versicolors and virginicas) are
3 types of flowers (ketoses, versicolored and virginic) are characterized according to the length and width of the sepal and petal.
sepal and petal
"""

# Download this dataset in the form of Pandas DataFrame using
# the seaborn library

import seaborn as sns
iris = sns.load_dataset('iris')
iris.head()


# Example of fitting and prediction of a supervised model

# Step 0: Generate Scikit-Learn data Let's consider a simple linear regression,
# i.e., the simplest case of fitting a line to the data on the (x, y) axes.

import matplotlib.pyplot as plt
import numpy as np

rng = np.random.RandomState(42)
x = 10 * rng.rand(50)
y = 2 * x - 1 + rng.rand(50)
plt.scatter(x, y)

# Step 1: Import the model
# In Scikit-Learn, each model is represented by a Python class

from sklearn.linear_model import LinearRegression

# Step 2: Configure the hyperparameters
# In Scikit-Learn, the hyperparameters are
# hyperparameters are chosen by passing values when you instantiate (when you define) the model

# create an instance of the LinearRegression class, and
# specify that we would like to incorporate the intercept using the fit_intercept hyperparameter.

modelo = LinearRegression(fit_intercept=True)
modelo

# Step 3: Structure the data into features and target vector

# Scikit-Learn, which requires a matrix of two-dimensional features and a one-dimensional
# two-dimensional features and a one-dimensional target vector. Here our target variable and
# is already in the correct form, but we need to model the data X to convert it into a matrix.
# In this case, this amounts to a simple reshaping of the one-dimensional matrix:

X = x[:, np.newaxis]
X.shape # dimensions of the new array ( rows, columns)

# Step 4: Adjust the model
# Now, it is necessary to adjust the parameters according to the data. This can be done with the
# fit( ) method of the model:
    
modelo.fit(X, y)

# The fit( ) command causes a series of internal model-dependent calculations to be executed, and the results of these calculations are stored in model-specific attributes that the user can store in the model.
# results of these calculations are stored in specific model attributes that the user can # explore and retrieve.
# explore and retrieve

# In Scikit-Learn, by convention, all model parameters learned during the training process
# learned during the training process have underscores at the end.

print(f"Coeficiente de la variable x : {modelo.coef_}")
print(f"Coeficiente de la variable x : {modelo.intercept_}")

# These two parameters represent the slope and the intersection of the linear model (the line).
# learned as a function of the data.

# Step 5: Predict

# Generate a set of values in an interval
xfit = np.linspace(-1, 11)

# Model the data
Xfit = xfit[:, np.newaxis]
# Prediction
yfit = modelo.predict(Xfit)
# Plots
plt.scatter(x, y)
plt.plot(xfit, yfit);
plt.show();



