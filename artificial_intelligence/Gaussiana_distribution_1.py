#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil

We will generate a data set from a Gaussian distribution and then train a Naive Bayes Gaussian model.
Gaussian distribution and then we will train a Naive Bayes Gaussian model. Let's see how to do
this in Scikit-learn
"""
# With this line we can display the graphs in Jupyter Notebook
# %matplotlib inline

import numpy as np
# Data visualization libraries 
import matplotlib.pyplot as plt
import seaborn as sns
# This we can generating data from a dataset
from sklearn.datasets import make_blobs

X, y = make_blobs(100, 2, centers=2, random_state=2, cluster_std=1.5)
plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap="RdBu");

# show graphyc
plt.show()

