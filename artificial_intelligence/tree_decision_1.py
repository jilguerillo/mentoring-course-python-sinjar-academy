#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil
"""

import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelEncoder
from IPython.display import Image

# Data
df = pd.read_csv('https://raw.githubusercontent.com/arundhaj/datamining_class/master/data/vertebrate.csv')
df.head()

# We replace all other types of animals with non-mammals.
df['Class Label'] = np.where(df['Class Label'] == 'mammal', 'mamifero', 'no-mamifero')

# We delete the variable name
df = df.drop(columns=['Name'])

# Separate into attributes and tags
X, y = df.iloc[:,:-1], df['Class Label']

# We encode cathegorical variables in numbers (One-Hot enconding).
X = X.apply(LabelEncoder().fit_transform)

# Training model
TREE = DecisionTreeClassifier(random_state=0, max_depth=3).fit(X,y)

print(TREE)