#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
One way to think of the K-means model is that it places a circle (or, in higher dimensions,
a hypersphere) at the center of each cluster, with a radius defined by the most distant point in the cluster.
cluster. This radius acts as a strict boundary for the assignment of observations within the
cluster: any point outside this circle is not considered a member of the cluster. We can visualize
this cluster model with the following function:
"""
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
# import model
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist

def plot_kmeans(kmeans, X, n_clusters = 4, rseed = 0, ax = None):
        labels = kmeans.fit_predict(X)

        # Plots the input data
        ax = ax or plt.fit_predict(X)
        ax.axis('equal')
        ax.scatter(X[:, 0], X[:, 1], c = labels, s = 40, cmap = 'viridis', zorder = 2)
        
        # Plots the representation of K-Means like circle
        centers = kmeans.cluster_centers_
        radii = [cdist(X[labels == i], [centers]).max()
                for i, center in enumerate(centers)]
        for c, r in zip(centers, radii):
                ax.add_patch(plt.Circle(c, r, fc='#CCCCCC', lw = 3, alpha = 0.5, zorder = 1))

# Model fits
kmeans = KMeans(n_clusters = 4, random_state = 0)
X, y_true = make_blobs(n_samples=300, centers=6,
                        cluster_std=0.60, random_state=0)

# adjust (which in this case means to iterate centroids)
kmeans.fit(X)

# Generate the assignments of each element to a centroid
y_kmeans = kmeans.predict(X)

# plot
plot_kmeans(kmeans, X)