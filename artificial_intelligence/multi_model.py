#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil
𝑦 = 𝑎 0 + 𝑎 1 𝑥 1 + 𝑎 2 𝑥 2 +. . . +𝑎 𝑛 𝑥 𝑛

variable, that is, multidimensional problems
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

rng = np.random.RandomState(1)
X = 10 * rng.randn(100, 3)
# generate regressors
y = 0.5 + np.dot(X, [1.5, -2., 1.])

# the model adjusts its parameters
# import the model
model = LinearRegression(fit_intercept=True)
model.fit(X, y)

print("X : ", X)
print("y : ", y)
print("Estimate the model : ", model.coef_)
print("Intercept : ", model.intercept_)


