#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil
pip install opencv-python
"""
import cv2
import numpy as np

# Reading video streaming
cap=cv2.VideoCapture(0)

# Determine ranges

redBajo1 = np.array([0,100,20], np.uint8)
redAlto1 = np.array([8,255,255], np.uint8)

redBajo2 = np.array([175,100,20], np.uint8)
redAlto2 = np.array([179,255,255], np.uint8)

while True:
    ret, frame=cap.read()
    if ret==True:
        # Opencv read default BGR
        # change image BGR to HSV
        frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        maskRed1 = cv2.inRange(frameHSV, redBajo1, redAlto1)
        maskRed2 = cv2.inRange(frameHSV, redBajo2, redAlto2)
        maskRed = cv2.add(maskRed1, maskRed2)
        # visualization of the colors as such in this case Red 
        maskRedVis = cv2.bitwise_and(frame, frame, mask= maskRed)
        
        cv2.imshow('maskRedVis', maskRedVis)
        # visualization of the obtained mask
        cv2.imshow('maskRed', maskRed)
        # visualization video streaming
        cv2.imshow('frame', frame)
        
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
cap.release()
cv2.destroyAllWindows()

