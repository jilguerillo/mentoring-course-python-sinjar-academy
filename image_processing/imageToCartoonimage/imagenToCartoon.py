'''
before you need install opencv :
pip3 install opencv-python

'''
# importing libraries
import cv2 

# Reading image in the path
img = cv2.imread("image.jpeg")

# processing images
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gray = cv2.medianBlur(gray, 5)
edges = cv2.adaptiveThreshold(gray, 255,
        cv2.ADAPTIVE_THRESH_MEAN_C,
        cv2.THRESH_BINARY, 9, 9)

# Cartoon image
color = cv2.bilateralFilter(img, 9, 250, 250)
cartoon = cv2.bitwise_and(color, color, mask=edges)

cv2.imshow("Image", img) # show original image
cv2.imshow("Edges", edges)
cv2.imshow("Cartoon", cartoon)

cv2.waitKey(2000)   # time for the shows images , 2 seconds = 2000
cv2.destroyAllWindows()