#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Creating a circle in frame video
@author: jil

pip install opencv-python

"""

import cv2
import numpy as np

cap = cv2.VideoCapture(0)
mask = np.zeros((480,640), dtype = np.uint8)
mask = cv2.circle(mask, (320, 240), 125, (255), -1)
#mask = cv2.bitwise_not(mask)

while (cap.isOpened()):
    ret, frame = cap.read()
    
    if ret==True:
        imgMask = cv2.bitwise_and(frame, frame, mask=mask)
        cv2.imshow('video', imgMask)
        
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
    else:
        break
    
cap.release()
cv2.destroyAllWindows()
