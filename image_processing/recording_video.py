#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Recording, save and read Video OPENCV
pip install opencv-python
"""
import cv2
captura = cv2.VideoCapture(0)
# outpu video
salida = cv2.VideoWriter('videoTest.avi', cv2.VideoWriter_fourcc(*'XVID'),20.0,(640,480))

#0xFF for 64 bit
while(captura.isOpened()):
    ret, imagen=captura.read()
    if ret == True:
        cv2.imshow('video', imagen)
        salida.write(imagen)
        # Stop capture press 's'
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
captura.release()
salida.release()
cv2.destroyAllWindows()


