#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script uses your camera. It was configured with the medium light, 
you can change the light setting 
to more or less bright

you need
pip install opencv-python
"""
import cv2
import numpy as np
    
video = cv2.VideoCapture(0)

# counter for the camera
i = 0  

while True:
    ret, frame = video.read()
    if ret == False: break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # method that waits for 20 images to be able to store
    if i == 20:
        bgGray = gray
    if i > 20:
        # subtraction of current image with background for motion detection
        diff = cv2.absdiff(gray, bgGray)
        # simple thresholding, gray to binary image
        _, th= cv2.threshold(diff, 40, 255, cv2.THRESH_BINARY)
        #opencv 4
        cnts,_= cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
        cv2.imshow('th', th)
        for c in cnts:
            area = cv2.contourArea(c)
            if area > 4000:
                x,y,w,h = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x+w, x+h), (0,255,0), 2)
                
    cv2.imshow('frame', frame)
    i += 1
    if cv2.waitKey(1) & 0xFF == ord ('x'):
        break

video.release()
cv2.destroyAllWindows()


