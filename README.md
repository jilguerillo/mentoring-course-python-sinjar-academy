# Mentoring Course Python

This repository is a collection of different exercises done in Python.

## Topics

- [ ] [Artificial Intelligence](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/artificial_intelligence) : Different python scripts, e.g. supervised models, decision tree, linear regression, k-means and gaussian distribution.

- [ ] [GUI](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/gui) : Script where user interface (GUI) is implemented.

- [ ] [Image Processing](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/image_processing) : Basic scripts e.g. face recognition, color detection and frame settings.

- [ ] [Microprocessor](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/microprocessor/System_DHT11_to_TelgramBot) : Implementation kits for microprocessors such as arduino and others.

- [ ] [Report Graphyc](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/report_graphyc) : Use of basic reports such as matplotlib and loading consoles.

- [ ] [Miscellaneous](https://gitlab.com/jilguerillo/mentoring-python-course/-/tree/main/miscellaneous) : Set of useful python scripts, for example, how to implement notifications in desktop, word counters, math with numpy, counter votes, among others.


### Remember

always to check the requirements to be able to run each of the scripts, either by downloading the modules or libraries in our environment for example.


