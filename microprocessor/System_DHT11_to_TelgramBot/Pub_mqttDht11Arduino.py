#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Jilguerillo
"""
import serial
#import numpy as np
import warnings
import paho.mqtt.client as mqtt
import time
import sys


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("DHT11 ")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic + "" + str(msg.payload.decode("utf-8")))

# Verify connection with Broker mqtt
try:
    print("Connected to MQTT Broker")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
except:
    print("MQTT Broker connection failed")

client.username_pw_set("dht11-python", "dht11-python")
client.connect("broker.shiftr.io", 1883, 60)

# Verify connection to dht11
try:
    print("Connected to DHT11")
    # check your dht11 input. example /dev/ttyUSB0
    arduino = serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout=1.0)
except:
    print("Connection failure with DHT11")

# Program cycle begins
with arduino:
    i = 0
    while True:
        val = input("Do you want to start the temperature reading? Type Yes or No:  ")
        if val.upper() == "yes".upper():
            val = 1
            i = 0
            arduino.write(str(val).encode())
            #while i < 10:
            while True:
                try:
                    # we read until we find the end of line
                    line = arduino.readline()
                    if not line:
                        # HACK: We discard empty lines because fromstring produces
                        # erroneous results, see
                        # https://github.com/numpy/numpy/issues/1714
                        continue
                except ValueError:
                    warnings.warn("Line {} didn't parse, skipping".format(line))
                except KeyboardInterrupt:
                    print("Exiting")
                    arduino.close()
                    break
                    exit()
                i += 1
                print(line.decode('utf-8'))
                client.publish("DHT11", line)
        elif val.upper() == "no".upper():
            try:
                print("Exiting")
                val = 0
                arduino.write(str(val).encode())
                arduino.close()
                exit()
            except KeyboardInterrupt:
                print("Exiting")
                arduino.close()
                exit()
        else:
            print("Invalid option")
            print("Exiting")
            arduino.close()
            exit()

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
exit()
