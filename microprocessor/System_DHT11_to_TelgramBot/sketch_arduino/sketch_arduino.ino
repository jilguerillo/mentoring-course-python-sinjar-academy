#include "DHT.h"
#define DHTPIN 2     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
    Serial.begin(9600);
    Serial.println(F("DHT11 Iniciado!"));

    dht.begin();  
}

void loop() {
  // Wait a few seconds between measurements.
    delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
    float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
    float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
    float f = dht.readTemperature(true);

  // Validación si alguna medición falló
    if (isnan(h) || isnan(t) || isnan(f)) {
        Serial.println(F("Falló del DHT sensor!"));
        return;
    }

  // Compute heat index in Fahrenheit (the default)
    float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
    float hic = dht.computeHeatIndex(t, h, false);

 //if (t > 9){
 //Espera de 5 seg. en cada medidición
    delay(5000);
  //Validación que envía 
    if (t > 30){
        Serial.print(F("| Alerta de temperatura alta. Código : 1al "));
        Serial.print(F("| Humedad: "));
        Serial.print(h);
        Serial.print(F("% | Temperatura: "));
        Serial.print(t);
        Serial.print(F("°C | "));
        Serial.print(f);
        Serial.println(F("°F |"));
    }
    else if (t < 0){
        Serial.print(F("Alerta de temperatura baja. Código : 1ba"));
        Serial.print(F("| Humedad: "));
        Serial.print(h);
        Serial.print(F("% | Temperatura: "));
        Serial.print(t);
        Serial.print(F("°C | "));
        Serial.print(f);
        Serial.println(F("°F |"));
    }
    else{
        Serial.print(F("| Humedad: "));
        Serial.print(h);
        Serial.print(F("% | Temperatura: "));
        Serial.print(t);
        Serial.print(F("°C | "));
        Serial.print(f);
        Serial.print(F("°F | Heat index: "));
        Serial.print(hic);
        Serial.print(F("°C | "));
        Serial.print(hif);
        Serial.println(F("°F | ")); 
    }
}
