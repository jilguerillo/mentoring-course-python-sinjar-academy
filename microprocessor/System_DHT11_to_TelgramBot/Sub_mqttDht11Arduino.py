#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Jilguerillo
"""

#import os
import serial
import sys
import paho.mqtt.client as mqtt
import smtplib
import requests
import time

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("DHT11")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic + "" + str(msg.payload.decode("utf-8")))
    line = str(msg.payload.decode("utf-8"))
    #if(line.count("1al") == 1):
    #time(5000)
    #val = "ENVIO DE ALERTA : " + line
    val = "Envío Temperatura : " + line
    print(send(val))

# Send alert for Telegram
def send(val):
    # Enter Telegram channel id and Token
    bot_token = ''
    bot_chatID = ''
    bot_message = val
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    return response.json()

# Verify connection with Broker mqtt
# You need to have user in some broker server
# example broker.shiftr.io
try:
    print("Connected to MQTT Broker")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
except:
    print("MQTT Broker connection failed")

client.username_pw_set("dht11-python", "dht11-python")
client.connect("broker.shiftr.io", 1883, 60)

client.loop_forever()
