#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: jil

Count the number of times each wor occurs in text (str). Return dictionary where 
keys are unique words and values are words counts.

"""

from collections import Counter

text = input("Ingresa texto para enumarar las palabras ingresadas: " )

def count_words(text):
    
    text = text.lower()
    skips =[".",",",";",":","'",'"']
    for ch in skips:
        text = text.replace(ch, "")
    
    word_counts = Counter(text.split(" "))
    return word_counts

print(count_words(text))