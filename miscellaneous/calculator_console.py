import sys

def __init__():      # FIRST CALL
    
    number_1 = 0
    number_2 = 0
    
    while True:
        print(Menu())
        option=int(input())
        if option == 1:         # Add
            Add()
        elif option == 2:       # Sustract
            Sustract()
        elif option == 3:       # Multiply
            Multiply()
        elif option == 4:       # Divide
            Divide()
        elif option == 5:       # Mathematical Power
            mPower()
        elif option == 6:       # Square root
            sRoot()
        elif option == 0:   # exit
            print(" ************* BYE ************")
            sys.exit()

def Add():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 + number_2
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************")
    
def Sustract():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 - number_2
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************") 

def Multiply():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 * number_2
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************")

def Divide():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 / number_2
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************")

def sRoot():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 ** (1/number_2)
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************")

def mPower():
    print("Enter number 1: ")
    number_1 = float(input())
    print("Enter number 2: ")
    number_2 = float(input())
    result =  number_1 ** number_2
    print(" *************************")
    print(f'* Result is: {result}   *' )
    print(" *************************")

def Menu():
    menu = """
        -------------------------------
        | 1.- Add                     |
        | 2.- Sustract                |
        | 3.- Multiply                |
        | 4.- Divide                  |
        | 5.- Mathematical Power      |
        | 6.- Square root             |
        | 0.- Exit                    |
        -------------------------------
        """
    return menu


#INIT APLICATION
__init__() 