#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Counts votes
"""

import random
import scipy.stats as ss

def majority_vote(votes):
    """
    Return the most common element in votes
    """
    vote_counts = {}
    for vote in votes:
        if vote in vote_counts:
            vote_counts[vote] += 1
        else:
            vote_counts[vote] = 1
            
    winners = []
    max_count = max(vote_counts.values())
    for vote, count in vote_counts.items():
        if count == max_count:
            winners.append(vote)
        
    return random.choice(winners)

def majority_vote_short(votes):
    """
    Return the most common element in votes
    """
    mode, count = ss.mstats.mode(votes)
        
    return mode

votes = [1,1,2,2,5,5,5,5,5,5,5,5,52,3,1,3,4,1,2,1,2,5,5,5,5,5,5,5,5,5,1,2,3,2,1,3,2,1,2,2,3,3,3,5,5,5,5,5,3,3,3,3,3,3,3]
winners = majority_vote_short(votes)

winners_2 = majority_vote(votes)

print(f"Winners with majority votes    : {winners}")
print(f"Winners with majority votes  2 : {winners_2}")

#max(vote_counts)
#max(vote_counts.keys())
#max(vote_counts.values())

