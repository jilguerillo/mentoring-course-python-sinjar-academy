#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jil
Find th distance between points p1 and p2
"""

import numpy as np

def distance(p1, p2):
    return np.sqrt(np.sum(np.power(p2 - p1, 2)))

p1 = np.array([1,1])
p2 = np.array([4,4])

distance(p1, p2)
print(f"Distance between point (array) {p1} and point (array) {p2} is : {distance(p1, p2)}")