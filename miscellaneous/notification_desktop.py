'''
use in terminal
pip3 install notification
or
pip3 install plyer
'''
import os
import platform
from plyer import notification # pip3 install notification in your terminal

# define a function and inside the function 
# we ask which operating system we are using
def notify(title , message ):  

    # we use the module platform and return string with the name system
    if platform.system() == 'Darwin':
        final_message = message + platform.system()
        os.system("osascript -e 'display notification \"{}\" with title \"{}\"'".format(title, final_message))
    
    elif platform.system() == 'Linux':
        notification.notify(
            title    = title,
            message  = message + platform.system(),
            app_name = 'My Python',
            timeout  = 30
            )
    # ...
    # Insert code for the other system

# Call function
notify('Hi welcome', 'your system is : ')

'''
title    : Notification title
message  : Notification message
app_name : name of the application that starts this notification
app_icon : icon to be displayed along with the message
timeout  : time to display the message, default value is 10
ticker   : text to be displayed in the status bar when the notification arrives
toast    : simple message instead of full notification
'''
