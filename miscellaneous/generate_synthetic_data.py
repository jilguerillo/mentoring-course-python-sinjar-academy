#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jilguerillo
generate synthetic data in PDF
"""

import scipy.stats as ss
import numpy as np
import matplotlib.pyplot as plt


def generate_synth_data(n=50):
    """Create two sets of points from bivariate normal distribution"""

    points = np.concatenate((ss.norm(0,1).rvs((5,2)), ss.norm(1,1).rvs((5,2))), axis=0)
    outcomes = np.concatenate((np.repeat(0, n), np.repeat(1, n)))
    return (points, outcomes)

n = 20
(points, outcomes) = generate_synth_data(n)

plt.figure()
plt.plot(points[:n,0], points[:n,1], "ro")
plt.plot(points[n:,0], points[n:,1], "bo")
plt.savefig("Bivardata.pdf") # you can change this name and path