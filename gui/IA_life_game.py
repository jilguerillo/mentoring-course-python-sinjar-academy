#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Python Game of Life Basic

The game uses a rectangular grid of cells of infinite size 
in which each cell is empty or occupied by an organism. 
It is said that occupied cells are alive, while empty ones 
are dead. The game is played over a specific period, with each 
turn creating a new “generation” based on the arrangement of 
living organisms in the current configuration.

you can live an automaton with the left mouse button 
and 
kill it with the right mouse button
"""

import pygame
import numpy as np
import time

# Create Display
pygame.init()
width, height = 700, 700 # Display width and height pixeles
screen = pygame.display.set_mode((height, width))

# Color Display 
bg = 25, 25, 25  # background color
screen.fill(bg)

# Create graphics
numxCell, numyCell = 50, 50

dimensionCellW = width / numxCell     # dimension Cells width
dimensionCellH = height / numyCell    # dimension Cells height

# Status of the cells. Lives = 1 ; Dead = 0;
gameState = np.zeros((numxCell, numyCell))

# Stick automaton
gameState[5, 3] = 1
gameState[5, 4] = 1
gameState[5, 5] = 1

# Mobile automaton
gameState[21, 21] = 1
gameState[22, 22] = 1
gameState[22, 23] = 1
gameState[21, 23] = 1
gameState[20, 23] = 1


# other
gameState[1, 6] = 1
gameState[1, 7] = 1
gameState[1, 8] = 1
gameState[1, 9] = 1
gameState[1, 10] = 1
gameState[1, 11] = 1
gameState[1, 12] = 1

# Execution control
pauseExect = False

while True:
    newGameState = np.copy(gameState) # saves every game update
            
    screen.fill(bg)
    time.sleep(0.1)
    
    # Register keyboard and mouse events
    ev = pygame.event.get()
            
    for event in ev:
        if event.type == pygame.KEYDOWN:
            pauseExect = not pauseExect
        
        # Detect if mouse is pressed
        mouseCLick = pygame.mouse.get_pressed()
        
        if sum(mouseCLick) > 0:
            positionX, positionY = pygame.mouse.get_pos()
            celdaX, celdaY = int(np.floor(positionX / dimensionCellW)), int(np.floor(positionY / dimensionCellH))
            newGameState[celdaX, celdaY] = not mouseCLick[2]
    
    for y in range(0, numxCell):
        for x in range(0, numyCell):
            
            if not pauseExect:
                        
                # Calculate the number of near neighbors
                n_neigh =   gameState[(x - 1)   % numxCell, (y - 1) % numyCell] + \
                            gameState[(x)       % numxCell, (y -1)  % numyCell] + \
                            gameState[(x + 1)   % numxCell, (y - 1) % numyCell] + \
                            gameState[(x - 1)   % numxCell, (y)     % numyCell] + \
                            gameState[(x + 1)   % numxCell, (y)     % numyCell] + \
                            gameState[(x - 1)   % numxCell, (y + 1) % numyCell] + \
                            gameState[(x)       % numxCell, (y + 1) % numyCell] + \
                            gameState[(x + 1)   % numxCell, (y + 1) % numyCell]
                            
                # Rule 1 : A dead cell with exactly 3 living neighbors "revives".
                if gameState[x, y] == 0 and n_neigh == 3:
                    newGameState[x, y] = 1
                
                # Rule 2 : A living cell with less than or more than 3 living neighbors "dies".
                elif gameState[x, y] == 1 and (n_neigh < 2 or n_neigh > 3):
                    newGameState[x, y] = 0
            
            # Coordinates of the defined rectangles
            # Multiply with index of each cell
            poly = [((x)    * dimensionCellW, y     * dimensionCellH),
                    ((x+1)  * dimensionCellW, y     * dimensionCellH),
                    ((x+1)  * dimensionCellW, (y+1) * dimensionCellH),
                    ((x)    * dimensionCellW, (y+1) * dimensionCellH)]
            
            # We draw the cell for each pair of x and y
            if newGameState[x, y] == 0:
                pygame.draw.polygon(screen, (128, 128, 128), poly, 1)
            else:
                
                pygame.draw.polygon(screen, (255, 255, 255), poly, 0)
    
    # We update the status
    gameState = np.copy(newGameState)
    
    # Update screen
    pygame.display.flip()

# Display undefined screen
#while True:
#    pass

