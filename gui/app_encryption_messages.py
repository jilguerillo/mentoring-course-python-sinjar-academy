# before import modules 
# pip3 install onetimepad

import onetimepad       
from tkinter import *

root = Tk()
root.title("Cryptography")   
root.geometry("500x200")      # widht and height of windows GUI

def encryptMessage():
    pt = e1.get()

    ct = onetimepad.encrypt(pt, "random")
    e2.insert(0, ct)

def decryptMessage():
    ct1 = e3.get()

    pt1 = onetimepad.decrypt(ct1, 'random')
    e4.insert(0, pt1)

### Creating the User interface ###

# positioning the grid and create labels
label_1 = Label(root, text= 'Plain text')
label_1.grid(row = 10, column = 1)
label_2 = Label(root, text = 'Encrypt Text')
label_2.grid(row = 11, column = 1)

label_3 = Label(root, text= 'Cipher text')
label_3.grid(row = 10, column = 10)
label_4 = Label(root, text= 'Decrypt text')
label_4.grid(row = 11, column = 10)

# Creating input
e1 = Entry(root)
e1.grid(row = 10, column = 2)
e2 = Entry(root)
e2.grid(row = 11, column = 2)

e3 = Entry(root)
e3.grid(row = 10, column = 11)
e4 = Entry(root)
e4.grid(row = 11, column = 11)

# Button action encryption
btn_encrypt = Button(root, text = "encrypt", bg = "red",
                    fg = "white", command = encryptMessage)
btn_encrypt.grid(row = 13, column = 2)

# Button action decryption
btn_decrypt = Button(root, text = "decrypt", bg = "green",
                    fg = "white", command = decryptMessage)
btn_decrypt.grid(row = 13, column = 11)

root.mainloop()